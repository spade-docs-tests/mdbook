# Getting started

This page will help you get started by compiling the compiler, writing some
Spade and uploading it to your (supported) board of choice.

## Installing Rust

Since the Spade compiler is written in Rust, begin by installing Rust. We
recommend using [rustup](https://rustup.rs), which manages different versions of
Rust for you. If you're on Linux it might be in your distributions package
repository, but you can also go to https://rustup.rs and follow the instructions
there.
