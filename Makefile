all: book
.PHONY: book open

book:
	mdbook build

open:
	mdbook serve --open
